﻿using UnityEngine;

public class PlatformSpawner : MonoBehaviour
{
    private const float LevelWidth = 5f;
    private const float Maxy = 1f;
    private const float Miny = .2f;

    public GameObject platform;
    public GameObject springPlatform;
    public GameObject player;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name.StartsWith("Platform"))
        {
            if (Random.Range(1, 10) == 1)
            {
                Destroy(collision.gameObject);
                Instantiate(springPlatform,
                    new Vector2(Random.Range(-LevelWidth, LevelWidth), player.transform.position.y + (37 + Random.Range(Miny, Maxy))),
                    Quaternion.identity);
            }
            else
            {
                collision.gameObject.transform.position = new Vector2(Random.Range(-LevelWidth, LevelWidth),
                    player.transform.position.y + (37 + Random.Range(Miny, Maxy)));
            }
        }
        else if (collision.gameObject.name.StartsWith("Spring"))
        {
            if (Random.Range(1, 10) == 1)
            {
                collision.gameObject.transform.position = new Vector2(Random.Range(-LevelWidth, LevelWidth),
                    player.transform.position.y + (37 + Random.Range(Miny, Maxy)));
            }
            else
            {
                Destroy(collision.gameObject);
                Instantiate(platform,
                    new Vector2(Random.Range(-LevelWidth, LevelWidth), player.transform.position.y + (37 + Random.Range(Miny, Maxy))),
                    Quaternion.identity);
            }
        }
    }
}
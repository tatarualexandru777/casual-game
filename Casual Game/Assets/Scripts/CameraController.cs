﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class CameraController : MonoBehaviour
{
    public Transform target;

    public void LateUpdate()
    {
        if (target.position.y > transform.position.y)
        {
            Vector3 position = transform.position;
            Vector3 newPos = new Vector3(position.x, target.position.y, position.z);
            position = newPos;
            transform.position = position;
        }

        if (target.position.y < transform.position.y - 60f) 
            SceneManager.LoadScene("Game Over");
    }
}